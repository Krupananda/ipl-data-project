#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 10 11:03:18 2019

@author: krupa
"""
from util import ipl_teams as shortform
import csv
import matplotlib.pyplot as plt
import sql_queries as query

def matches_win(file_path):
    match_tracker = {}
    seasons = []
    with open(file_path, 'r') as csv_file:
        ipl_data = csv.DictReader(csv_file)
        for year in ipl_data:
            winner, season = year["winner"], year["season"]
            seasons.append((season))
            if winner in match_tracker:
                if season in match_tracker[winner]:
                    match_tracker[winner][season] += 1
                else:
                    match_tracker[winner][season] = 1
            else:
                match_tracker[winner] = {}
                match_tracker[winner][season] = 1

        if "" in match_tracker:
            del match_tracker[""]
        return list(set(seasons)), match_tracker


def zero_wins(seasons, match_data):
    teams = list(match_data.keys())
    seasons.sort()
    for i in range(len(teams)):
        for j in range(len(seasons)):
            if seasons[j] not in match_data[teams[i]]:
                match_data[teams[i]][seasons[j]] = 0
    return seasons, teams, match_data


def plot_graphs(seasons, teams, match_data):
    colors = {'Chennai Super Kings': "red",
              'Deccan Chargers': "blue",
              'Delhi Daredevils': 'black',
              'Gujarat Lions': 'orange',
              'Kings XI Punjab': 'green',
              'Kochi Tuskers Kerala': 'yellow',
              'Kolkata Knight Riders': 'pink',
              'Mumbai Indians': 'violet',
              'Pune Warriors': 'brown',
              'Rajasthan Royals': 'purple',
              'Royal Challengers Bangalore': 'magenta',
              'Sunrisers Hyderabad': '#FF7F50'}
    short_names = []
    for team in teams:
        short_names.append(shortform(team))
    plt.xlabel("Years")
    plt.ylabel("Wins")
    plt.axis([2006, 2020, 0, 100])
    plt.title("Team Wins")
    for season in seasons:
        btm = 0
        for team in teams:
            plt.bar(int(season), match_data[team][
                    season], color=colors[team], bottom=btm, width=0.5)
            btm += match_data[team][season]
    plt.legend(short_names, ncol=5)
    plt.show()


def matches_won_all_teams_in_Ipl(file_path):
#    seasons, ipl_data = matches_win(file_path)
    seasons, ipl_data = query.team_wins()
    seasons_list, teams, winners_data = zero_wins(seasons, ipl_data)
    plot_graphs(seasons_list, teams, winners_data)
if __name__ == "__main__":
    matches_won_all_teams_in_Ipl("matches.csv")
