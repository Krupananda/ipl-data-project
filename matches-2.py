import matplotlib.pyplot as plt
import csv
def plot_graph(x,y):
        plt.bar(x,y)
        plt.xlabel('Years', fontsize=18)
        plt.ylabel('No Of Matches', fontsize=16)
        plt.axis([2007,2020,1,100])
        plt.title("Matches In a Year",fontsize=20)
        plt.show()

def get_ipl_data(file):
    extracted_data={}
    with open(file ,'r') as csv_file:
        ipl_data=csv.DictReader(csv_file)
        for year in ipl_data:
            if year["season"]  in extracted_data:
                extracted_data[year["season"]]+=1
            else:
                extracted_data[year["season"]]=1
    data=list(extracted_data.keys())
    matches=list(extracted_data.values())
    years=[]
    for i in data:
        years.append(int(i))
    return plot_graph(years,matches)   


get_ipl_data('matches.csv')


        
