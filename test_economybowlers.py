import unittest
import economybowlers as bowl
class test_economy_bowlers(unittest.TestCase):
    def test_economy(self):
        runs,balls=bowl.bowlers_economy("economy_bowlers_matches.csv","economy_bowlers_deliveries.csv")
        result=bowl.economy_per_over(runs,balls)
        expected_output={'UT Yadav':4,'M Morkel':5}
        self.assertEqual(result,expected_output)
if __name__=="__main__":
    unittest.main()
    