#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 14:47:36 2019

@author: krupa
"""
import util
import psycopg2 as db
def super_over_matches():
    try:
        con=db.connect(database='pythondb',user='postgres',host='localhost',password='krupa')
        cur=con.cursor()
        cur.execute('''select season,count(result) from matches 
                    where result='tie'
                    group by season;''')
        result=cur.fetchall()
        return util.tup_to_dic(result)
       
    except db.Error as e:
        print("Error while connecting to database",e)
def matches_played():
    try:
        years,wins=[],[]
        con=db.connect(database='pythondb',user='postgres',host='localhost',password='krupa')
        cur=con.cursor()
        cur.execute('''select season,count(season) from matches
                    group by season;''')
        seasons=cur.fetchall()
        for season in seasons:
            years.append(int(season[0]))
            wins.append(season[1])
        return years,wins
    except db.Error as e:
        print ("Error while connecting to PostgreSQL",e)
    finally:
        con.close();
def team_wins():
    match_tracker={}
    years=[]
    try:
        conn=db.connect(database="pythondb",host="localhost",user="postgres",password="krupa")
        cur=conn.cursor()
        cur.execute('''select winner,season,count(winner) from matches
                    group by winner,season;''')
        ipl_data=cur.fetchall()
        for team in ipl_data:
            year,wins=team[1],team[2]
            years.append(year)
            if team[0] in match_tracker:
                match_tracker[team[0]][year]=wins
            else :
                match_tracker[team[0]]={}
                match_tracker[team[0]][year]=wins
        if None in match_tracker:
            del match_tracker[None]
        return list(set(years)),match_tracker          
    except db.Error as e:
        print("Error while connecting postgresql",e)
    finally:
        conn.close();
def extra_runs():
    try:
        con=db.connect(database="pythondb",user="postgres",password="krupa",host="localhost")
        cur=con.cursor()
        cur.execute("""select bowling_team,sum(extra_runs) from deliveries
                       where match_id in (select matches.id from matches where matches.season='2016')
                       group by bowling_team; """)
        extras=cur.fetchall();
        return util.tup_to_dic(extras)
    except db.Error as e:
        print("Error while connecting to postgreSQL",e )
    finally:
        con.close()
def bowlers_economy():
    try:
        con=db.connect(database='pythondb',user='postgres',password='krupa',host='localhost')
        cur=con.cursor()
        cur.execute(''' select bowler,sum(total_runs) from deliveries
                    where match_id in (select id from matches where season='2015')
                    group by bowler;''')
        runs=util.tup_to_dic(cur.fetchall())
        cur.execute(''' select bowler,sum(ball) from deliveries
                    where match_id in (select id from matches where season='2015') and ball<7
                    group by bowler;''')
        balls=util.tup_to_dic(cur.fetchall())
        return runs,balls
    except db.Error as e:
        print("Error while connecting to database",e)
        
        
        