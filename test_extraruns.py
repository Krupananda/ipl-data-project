import extraruns as extras
import unittest


class test_extra_runs(unittest.TestCase):

    def test_runs(self):
        runs_data = extras.extra_runs(
            'test_extraruns_matches.csv', 'test_extraruns_deliveries.csv')
        teams, runs = extras.team_runs(runs_data)
        result = [teams, runs]
        expected_output = [['Sunrisers Hyderabad'], [4]]
        self.assertEqual(result, expected_output)
if __name__ == "__main__":
    unittest.main()
