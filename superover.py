import matplotlib.pyplot as plt
import csv


import sql_queries as query 

def super_overs(matches_path):
    match_tracker = {}
    with open(matches_path, 'r') as csv_matches_path:
        ipl_data = csv.DictReader(csv_matches_path)
        for data in ipl_data:
            if data['result'] == 'tie':
                if data['season'] not in match_tracker:
                    match_tracker[data['season']] = 1
                else:
                    match_tracker[data['season']] += 1
    return match_tracker


        
def plot_graphs(match_tracker):
   
    years = sorted(list(match_tracker))
    x,y=[],[]
    for year in years:
        x.append(int(year))
    for year in years:
        y.append(match_tracker[year])
    plt.bar(x, y)
    plt.xlabel('Years')
    plt.xticks(list(range(2009, 2018, 1)))
    plt.yticks(y)
    plt.ylabel('matches')
    plt.title('No of Superovers matches in IPL')
    plt.show()


def ipl_matches(matches_path):
    matches = super_overs(matches_path)
#    matches=query.super_over_matches()
    plot_graphs(matches)
if __name__ == "__main__":
    ipl_matches('matches.csv')
