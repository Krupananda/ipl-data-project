import superover as over
import unittest


class test_superover(unittest.TestCase):

    def test_overs(self):
        result = over.super_overs("test_matches.csv")
        expected_output = {2017: 1}
        self.assertEqual(result, expected_output)
if __name__ == "__main__":
    unittest.main()
