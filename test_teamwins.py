import unittest
import teamwins


class test_team_wins(unittest.TestCase):

    def test_wins(self):
        year, wins = teamwins.matches_win("test_matches.csv")
        result = [year, wins]
        match_tracker = {'Sunrisers Hyderabad': {'2017': 8},
                         'Royal Challengers Bangalore': {'2017': 3},
                         'Mumbai Indians': {'2017': 12},
                         'Pune Warriors': {'2017': 10},
                         'Gujarat Lions': {'2017': 4},
                         'Kolkata Knight Riders': {'2017': 9},
                         'Kings XI Punjab': {'2017': 7},
                         'Delhi Daredevils': {'2017': 6}
                         }
        expected_output = [['2017'], match_tracker]
        self.assertEqual(result, expected_output)
if __name__ == "__main__":
    unittest.main()
