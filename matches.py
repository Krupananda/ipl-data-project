import matplotlib.pyplot as plt
import csv
import sql_queries as query

def plot_graph(x, y):
    plt.bar(x, y)
    plt.xlabel('Years', fontsize=18)
    plt.ylabel('No Of Matches', fontsize=16)
    plt.axis([2007, 2020, 1, 100])
    plt.xticks(x)
    plt.title("Matches In a Year", fontsize=20)
    plt.show()


def matches_win(file_path):
    extracted_data = {}
    with open(file_path, 'r') as csv_file:
        ipl_data = csv.DictReader(csv_file)
        for year in ipl_data:
            season = year["season"]
            if season in extracted_data:
                extracted_data[season] += 1
            else:
                extracted_data[season] = 1
    matches = list(extracted_data.values())
    years = []
    for year in list(extracted_data.keys()):
        years.append(int(year))
    return years, matches


  
def total_matches_in_ipl(file_path):
#    years, matches = matches_win('matches.csv')
    years,matches=query.matches_played()
    plot_graph(years, matches)      

if __name__ == "__main__":
    total_matches_in_ipl("matches.csv")
