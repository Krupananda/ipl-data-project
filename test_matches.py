import matches
import unittest


class test_matches_wins(unittest.TestCase):

    def test_wins(self):
        years, total_matches = matches.matches_win("test_matches.csv")
        result = [years, total_matches]
        print(result)
        expected_output = [[2017], [59]]
        self.assertEqual(result, expected_output)
        
if __name__ == "__main__":
    unittest.main()
