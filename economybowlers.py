import matplotlib.pyplot as plt
import csv
import sql_queries as query


def bowlers_economy(matches_path, deliveries_path):
    match_id = []
    runs_tracker = {}
    ball_tracker = {}
    with open(matches_path, 'r') as csv_file:
        id_data = csv.DictReader(csv_file)
        for data in id_data:
            if data["season"] == '2015':
                match_id.append(data["id"])

    with open(deliveries_path, 'r') as f:
        ipl_data = csv.DictReader(f)
        for data in ipl_data:
            if data["match_id"] in match_id:
                if data["bowler"] not in runs_tracker:
                    runs_tracker[data["bowler"]] = int(data["total_runs"])
                else:
                    runs_tracker[data["bowler"]] += int(data["total_runs"])
                if data["bowler"] not in ball_tracker:
                    if int(data["wide_runs"]) > 0 or int(data["noball_runs"]) > 0:
                        ball_tracker[data["bowler"]] = 0
                    else:
                        ball_tracker[data["bowler"]] = 1
                else:
                    if int(data["wide_runs"]) <= 0 or int(data["noball_runs"]) <= 0:
                        ball_tracker[data["bowler"]] += 1
    return runs_tracker, ball_tracker


def economy_per_over(runs_tracker, ball_tracker):
    avg_runs = {}
    for balls in ball_tracker:
        ball_tracker[balls] = ball_tracker[balls] / 6
    for name in runs_tracker:
        avg_runs[name] = (runs_tracker[name]) / ball_tracker[name]
    economy_balls = sorted(list(avg_runs.values()))[:5]
    top_5_bowlers = {}
    for data in avg_runs:
        if avg_runs[data] in economy_balls:
            top_5_bowlers[data] = avg_runs[data]
    return top_5_bowlers


def plot_graph(top_5_bowlers):
    economy = sorted(list(top_5_bowlers.values()))
    bowlers = list(top_5_bowlers.keys())
    x = []
    for data in economy:
        for bowler in bowlers:
            if data == top_5_bowlers[bowler]:
                pos = economy.index(data)
                x.insert(pos, bowler)
                break

    plt.bar(x, economy, color=['black', 'red', 'green', 'blue', 'cyan'])
    plt.xlabel("Bowlers")
    plt.ylabel("Economy")
    plt.xticks(rotation=15)
    plt.title("Top Five Economy Bowlers in 2015")
    plt.show()


def ipl_bowlers_economy(matches_path, deliveries_path):
#    runs, balls = bowlers_economy(matches_path, deliveries_path)
    runs, balls = query.bowlers_economy()

    top_bowlers = economy_per_over(runs, balls)
    plot_graph(top_bowlers)
if __name__ == "__main__":
    ipl_bowlers_economy("matches.csv", "deliveris.csv")
