def ipl_teams(team):
    teams = {'Chennai Super Kings': 'CSK',
             'Deccan Chargers': 'DC',
             'Delhi Daredevils': 'DD',
             'Gujarat Lions': 'GL',
             'Kings XI Punjab': 'KXIP',
             'Kochi Tuskers Kerala': 'KTK',
             'Kolkata Knight Riders': 'KKR',
             'Mumbai Indians': 'MI',
             'Pune Warriors': 'PW',
             'Rajasthan Royals': 'RR',
             'Royal Challengers Bangalore': 'RCB',
             'Sunrisers Hyderabad': 'SRH'}
    return teams[team]
def tup_to_dic(tup):
    tracker={}
    for data in tup:
        key,val=data
        tracker[key]=val;
    return tracker
