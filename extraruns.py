#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 10 19:22:37 2019

@author: krupa
"""
import matplotlib.pyplot as plt
import csv
from util import ipl_teams as shortname
import sql_queries as query


def extra_runs(matches_path, deliveries_path):
    match_id = []
    match_tracker = {}
    with open(matches_path, 'r') as f:
        raw_data = csv.DictReader(f)
        for data in raw_data:
            if data["season"] == '2016':
                match_id.append(data["id"])
    with open(deliveries_path, 'r') as csv_file:
        deliveries_data = csv.DictReader(csv_file)
        for data in deliveries_data:
            if data["match_id"] in match_id:
                if data["bowling_team"] in match_tracker:
                    match_tracker[data["bowling_team"]
                                  ] += int(data["extra_runs"])
                else:
                    match_tracker[data["bowling_team"]] = int(
                        data["extra_runs"])

    return match_tracker


def team_runs(match_tracker):
    teams = list(match_tracker.keys())
    runs = []
    for i in teams:
        runs.append(match_tracker[i])
    return teams, runs


def plot_graphs(teams, runs):

    short_names = []
    for team in teams:
        short_names.append(shortname(team))
    plt.bar(short_names, runs, width=0.9)
    plt.xlabel("Teams")
    plt.ylabel("Extra Runs")
    plt.title("Extra Runs given by Each Team")
    plt.show()


def ipl_extra_runs(matches_path, deliveries_path):
#    ipl_data = extra_runs(matches_path, deliveries_path)
    ipl_data=query.extra_runs()
    teams, runs = team_runs(ipl_data)
    plot_graphs(teams, runs)

if __name__ == "__main__":
    ipl_extra_runs("matches.csv", "deliveries.csv")
